<?php
require('lib/common.php');

$twig = twigloader();

$newsid = (isset($_GET['id']) ? $_GET['id'] : 0);

if ($newsid) {
	$newsdata = fetch("SELECT * FROM news WHERE id = ?", [$newsid]);

	if (!$newsdata) {
		error('404', "The requested news article wasn't found.");
	}

	if (!isset($newsdata['redirect'])) {
		clearMentions('index', $newsid);
		
		$time = date('jS F Y', $newsdata['time']).' at '.date('H:i:s', $newsdata['time']);
		
		$markdown = new Parsedown();
		$newsdata['text'] = $markdown->text($newsdata['text']);

		$comments = query("SELECT $userfields c.* FROM comments c JOIN users u ON c.author = u.id WHERE c.type = 2 AND c.level = ? ORDER BY c.time DESC", [$newsid]);

		echo $twig->render('index.twig', [
			'newsid' => $newsid,
			'news' => $newsdata,
			'time' => $time,
			'comments' => $comments
		]);
	} else {
		redirect($newsdata['redirect']);
	}
} else {
	$newsdata = query("SELECT * FROM news ORDER BY id DESC");

	echo $twig->render('index.twig', [
		'newsid' => $newsid,
		'news' => $newsdata,
	]);
}